import firebase from 'firebase';

const firebaseConfig = {
  apiKey: "AIzaSyBsBaH1avJwcy1KVi6MDtfCAlXIty6TS-E",
  authDomain: "chousei-firebase-273ba.firebaseapp.com",
  databaseURL: "https://chousei-firebase-273ba.firebaseio.com",
  projectId: "chousei-firebase-273ba",
  storageBucket: "",
  messagingSenderId: "617438072521",
  appId: "1:617438072521:web:3e183806bd6f3d6b0bc718"
};

export const firebaseApp = firebase.initializeApp(firebaseConfig);
